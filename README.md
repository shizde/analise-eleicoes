# analise-eleicoes



## 1.Objetivo

Este trabalho tem como objetivo avaliar as eleicoes de 2016 e 2020 para vereador em Vitoria - ES.


## 2.Metodo

### 2.1 Utilitarios

- Para a primeira versao e analise gerada apenas sera utilizada a biblioteca Pandas.
- Serao utilizados ficheiros disponibilizados pelo site do TSE para analise dos dados.
- Sera utilizado um ficheiro com os dados partidarios.

## 3.Desenvolvimento

#### 3.1 Datasource
Para esta analise, 3 ficheiros sao necessarios:
- tabela com nomes, siglas e numeros dos partidos
- resultado das eleicoes municipais de 2016
- resultado das eleicoes municipais de 2016

#### 3.2 Script
O script gerado contem uma funcao que trabalha toda a interface dos dados ja que esta deve estar parametrizada para ambos os anos, gerando resultados analogos a partir dos dados individuais. A partir disto passos especificos sao performados:

1. Filtrar colunas relevantes:
```
df = df[['NM_VOTAVEL','NR_VOTAVEL','ANO_ELEICAO','DS_CARGO','NM_MUNICIPIO','QT_VOTOS']]

```
2. Adicionar uma coluna com a sigla partidaria e popular
```
df['SG_PART'] = ''
for partido in partidos.itertuples(index=True):
    for candidato in df.itertuples(index=True):
        if str(candidato.NR_VOTAVEL)[:2] == str(partido.NR_PART):
            df.at[candidato.Index, 'SG_PART'] = partido.SG_PART
```
3. Ordenar headers
```
df = df[['NM_VOTAVEL','NR_VOTAVEL','SG_PART','ANO_ELEICAO','DS_CARGO','NM_MUNICIPIO','QT_VOTOS']]
df = df.rename(columns={"NM_VOTAVEL": "Candidato", "NR_VOTAVEL": "Numero de Candidato", 
                        "SG_PART": "Partido", "DS_CARGO": "Cargo", "NM_MUNICIPIO": "Municipio", "QT_VOTOS": "Votos"}, errors="raise")
```

4. Estabelecer relacao para Vitoria / Vereador
```
df = df[df['Municipio'] == 'VITÓRIA']
options = ['Vereador', 'VEREADOR']
df = df[df['Cargo'].isin(options) ]
```

5. Agregar dados por candidato, somando os votos de cada
```
aggregation_functions = {'Numero de Candidato': 'first','Partido': 'first','ANO_ELEICAO': 'first', 'Cargo': 'first','Municipio': 'first','Votos': 'sum'}
df = df.groupby(df['Candidato'], as_index=False).aggregate(aggregation_functions)
```

6. Ordenar por Partido e mais votado
```
df = df.sort_values(by=['Partido','Votos'], ascending=False, ignore_index=True)
```

## 4.Resultado

Os resultados obtidos podem ser encontrados no ficheiro ```analise_eleicoes.xlsx```


